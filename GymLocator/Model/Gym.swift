//
//  Gym.swift
//  GymLocator
//
//  Created by Krish on 20/12/18.
//  Copyright © 2018 krish. All rights reserved.
//

import Foundation
import SwiftyJSON

class Gym {
    var name: String
    var address: String
    init(json: JSON) {
        self.name = json["name"].string ?? ""
        self.address = json["address"].string ?? ""
    }
}
