//
//  GymListViewModel.swift
//  GymLocator
//
//  Created by Krish on 20/12/18.
//  Copyright © 2018 krish. All rights reserved.
//

import Foundation
import CoreLocation

struct GymListViewModel {
    let gymService: GymServiceProtocol
    var successCallback: (([Gym]?) -> Void)?
    var failureCallback: ((String?) -> Void)?
    
    var gyms: [Gym]? {
        didSet {
            guard let ggyms  = gyms else {
                self.failureCallback?("no data found")
                return
            }
            self.successCallback?(ggyms)
            self.gyms = ggyms
        }
    }
    
    init(gymService: GymServiceProtocol) {
        self.gymService = gymService
    }
    
    
     func fetchForLocation(withLocation location: CLLocationCoordinate2D) {
        gymService.getGyms(fromLocation: location, onSuccess: {  data in
            var weakSelf :GymListViewModel? = self
            if let data = data {
                weakSelf?.gyms = data
            }
        }) { error in
            let weakSelf :GymListViewModel? = self
            weakSelf?.failureCallback?(error)
        }
    }

}
